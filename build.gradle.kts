buildscript {
    repositories {
        gradlePluginPortal()
    }
}

plugins {
    checkstyle
}

apply<BootstrapPlugin>()
apply<VersionPlugin>()

allprojects {
    group = "com.openosrs"
    version = ProjectVersions.rlVersion
    apply<MavenPublishPlugin>()
}

subprojects {
    group = "com.openosrs.release"

    project.extra["PluginProvider"] = "xKylee"
    project.extra["ProjectUrl"] = "https://discord.gg/meGjpjm"
    project.extra["PluginLicense"] = "3-Clause BSD License"

    repositories {
        maven {
            url = uri("https://dl.bintray.com")
        }

        jcenter {
            content {
                excludeGroupByRegex("com\\.openosrs.*")
                excludeGroupByRegex("net\\.runelite.*")
                excludeGroupByRegex("com\\.runelite.*")
            }
        }

        exclusiveContent {
            forRepository {
                maven {
                    url = uri("https://bitbucket.org/openosrsjava8/openosrs-base-plugins-hosting/raw/master/")
                    artifactUrls("https://bitbucket.org/openosrsjava8/openosrs-base-plugins-hosting/raw/master/")
                    metadataSources {
                        artifact()
                    }
                }
            }
            filter {
                includeGroup("com.openosrs.release")
//                includeGroupByRegex("com\\.openosrs\\.release.*")
            }
        }


        exclusiveContent {
            forRepository {
                maven(url = "https://bitbucket.org/openosrsjava8/hosting/raw/master/")
            }
            filter {
                includeGroup("com.openosrs")
                includeGroup("com.openosrs.rs")
                includeGroup("com.openosrs.rxrelay3")
            }
        }

        exclusiveContent {
            forRepository {
                maven {
                    url = uri("https://repo.runelite.net")
                }
            }
            filter {
                includeGroupByRegex("net\\.runelite.*")
            }
        }
        maven(url = "https://jitpack.io")
    }

    apply<JavaPlugin>()
    apply(plugin = "checkstyle")

    checkstyle {
        maxWarnings = 0
        toolVersion = "8.25"
        isShowViolations = true
        isIgnoreFailures = false
    }

    configure<PublishingExtension> {
        repositories {
            maven {
                url = uri("$buildDir/repo")
            }
        }
        publications {
            register("mavenJava", MavenPublication::class) {
                from(components["java"])
            }
        }
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    tasks {
        withType<JavaCompile> {
            options.encoding = "UTF-8"
        }

        withType<Jar> {
            doLast {
                copy {
                    from("./build/libs/")
                    into("../release/")
                }
            }
        }

        withType<AbstractArchiveTask> {
            isPreserveFileTimestamps = false
            isReproducibleFileOrder = true
            dirMode = 493
            fileMode = 420
        }

        withType<Checkstyle> {
            group = "verification"

            exclude("**/ScriptVarType.java")
            exclude("**/LayoutSolver.java")
            exclude("**/RoomType.java")
        }

        register<Copy>("copyDeps") {
            into("./build/deps/")
            from(configurations["runtimeClasspath"])
        }
    }
}
